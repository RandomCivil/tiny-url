package tinyurl

import "errors"

const (
	ZK_APPNAME_RANGE_MAP_URL = "/app_range_map"
	ZK_COUNTER_URL           = "/counter"
	ZK_COUNTER_RANGE_URL     = ZK_COUNTER_URL + "/range_"
	LIMIT_PER_RANGE          = 100
)

const (
	ETCD_COUNTER_URL     = "/counter/"
	ETCD_RANGE           = "range_%d"
	ETCD_RANGE_COUNT_KEY = "rangeCount"
)

var ETCD_CAS_ERR = errors.New("etcd cas error")

var Base62Arr = [62]string{
	"0",
	"1",
	"2",
	"3",
	"4",
	"5",
	"6",
	"7",
	"8",
	"9",
	"a",
	"b",
	"c",
	"d",
	"e",
	"f",
	"g",
	"h",
	"i",
	"j",
	"k",
	"l",
	"m",
	"n",
	"o",
	"p",
	"q",
	"r",
	"s",
	"t",
	"u",
	"v",
	"w",
	"x",
	"y",
	"z",
	"A",
	"B",
	"C",
	"D",
	"E",
	"F",
	"G",
	"H",
	"I",
	"J",
	"K",
	"L",
	"M",
	"N",
	"O",
	"P",
	"Q",
	"R",
	"S",
	"T",
	"U",
	"V",
	"W",
	"X",
	"Y",
	"Z",
}
