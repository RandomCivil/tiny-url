package core

import (
	"strings"

	tinyurl "gitlab.com/RandomCivil/tiny-url"
)

func Base62(n int64) string {
	if n == 0 {
		return "0"
	}
	var result []string
	for n > 0 {
		result = append([]string{tinyurl.Base62Arr[n%62]}, result...)
		n /= 62
	}
	return strings.Join(result, "")
}
