package core

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestBase62(t *testing.T) {
	tests := []struct {
		n      int64
		expect string
	}{
		{
			n:      0,
			expect: "0",
		},
		{
			n:      1,
			expect: "1",
		},
		{
			n:      2,
			expect: "2",
		},
		{
			n:      10,
			expect: "a",
		},
		{
			n:      61,
			expect: "Z",
		},
		{
			n:      62,
			expect: "10",
		},
		{
			n:      124,
			expect: "20",
		},
		{
			n:      1000,
			expect: "g8",
		},
		{
			n:      10000,
			expect: "2Bi",
		},
	}
	Convey("TestBase62", t, func() {
		for _, tt := range tests {
			So(Base62(tt.n), ShouldEqual, tt.expect)
		}
	})
}
