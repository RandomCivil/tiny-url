## tiny url generator

### zk design

```mermaid
flowchart LR
    /counter --- /counter/range_1 -- 临时顺序节点 --- 1-100
    /counter --- /counter/range_2 -- 临时顺序节点 --- 101-200
    /counter --- /counter/range_3 -- 临时顺序节点 --- 201-300
```

```mermaid
flowchart LR
    /app_range_map
    /app_range_map --- /app_range_map/app_id1 --- data:range_1
    /app_range_map --- /app_range_map/app_id2 --- data:range_2
    /app_range_map --- /app_range_map/app_id3 --- data:range_3
```

createRange流程

```mermaid
flowchart TD
    id1("GET /counter") --> id2("create /counter/range_(childrenNum+1)") --> id3("set /app_range_map/app_id range_(childrenNum+1)")
```

### etcd design

```mermaid
flowchart LR
    /counter/app1/range1 --- 1
    /counter/app1/range1 --- ...
    /counter/app1/range1 --- 100

    /counter/app1/range2 --- 101
    /counter/app1/range2 --- 1...
    /counter/app1/range2 --- 200

    /counter/app1/range3 --- 201
    /counter/app1/range3 --- 2...
    /counter/app1/range3 --- 300

    /counter/app2/range4 --- 301
    /counter/app2/range4 --- 3...
    /counter/app2/range4 --- 400

    /counter/app3/range5 --- 401
    /counter/app3/range5 --- 4...
    /counter/app3/range5 --- 500
```
