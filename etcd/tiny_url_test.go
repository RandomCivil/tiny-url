package etcd

import (
	"context"
	"fmt"
	"sort"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	tinyurl "gitlab.com/RandomCivil/tiny-url"
	etcdClient "go.etcd.io/etcd/client/v3"
	"go.etcd.io/etcd/client/v3/concurrency"
	"golang.org/x/sync/errgroup"
)

func TestGetCurRange(t *testing.T) {
	appName := "test1"
	tu, err := NewTinyUrl(appName, []string{"127.0.0.1:2379"})
	if err != nil {
		t.Fatal(err)
	}
	prefixKey := tinyurl.ETCD_COUNTER_URL + appName

	Convey("TestGetCurRange", t, func() {
		Convey("不存在range", func() {
			resp, err := tu.etcdCli.Delete(tu.ctx, prefixKey, etcdClient.WithPrefix())
			if err != nil {
				t.Fatal(err)
			}
			t.Log("prefixKey Deleted", resp.Deleted)

			resp, err = tu.etcdCli.Delete(tu.ctx, tinyurl.ETCD_RANGE_COUNT_KEY)
			if err != nil {
				t.Fatal(err)
			}
			t.Log("ranngeCount Deleted", resp.Deleted)

			curRange, err := tu.getCurRange()
			So(curRange, ShouldEqual, prefixKey+"/range_1")
			So(err, ShouldBeNil)

			resp1, err := tu.etcdCli.Get(tu.ctx, tinyurl.ETCD_RANGE_COUNT_KEY)
			So(string(resp1.Kvs[0].Value), ShouldEqual, "1")
			So(err, ShouldBeNil)
		})
		Convey("存在一个range", func() {
			resp, err := tu.etcdCli.Put(tu.ctx, prefixKey+"/range_1", "1")
			if err != nil {
				t.Fatal(err)
			}
			t.Log("put", resp.PrevKv, resp.Header.GetRevision())

			curRange, err := tu.getCurRange()
			So(curRange, ShouldEqual, prefixKey+"/range_1")
			So(err, ShouldBeNil)
		})

		Convey("存在多个range", func() {
			for i := 2; i <= 5; i++ {
				resp, err := tu.etcdCli.Put(tu.ctx, fmt.Sprintf(prefixKey+"/range_%d", i), fmt.Sprintf("%d", i))
				if err != nil {
					t.Fatal(err)
				}
				t.Log("put", resp.PrevKv, resp.Header.GetRevision())
			}

			curRange, err := tu.getCurRange()
			So(curRange, ShouldEqual, prefixKey+"/range_5")
			So(err, ShouldBeNil)
		})
	})
}

func TestShortUrl(t *testing.T) {
	appName := "test2"
	tu, err := NewTinyUrl(appName, []string{"127.0.0.1:2379"})
	if err != nil {
		t.Fatal(err)
	}
	prefixKey := tinyurl.ETCD_COUNTER_URL + appName

	resp, err := tu.etcdCli.Delete(tu.ctx, prefixKey, etcdClient.WithPrefix())
	if err != nil {
		t.Fatal(err)
	}
	t.Log("Deleted", resp.Deleted)

	resp, err = tu.etcdCli.Delete(tu.ctx, tinyurl.ETCD_RANGE_COUNT_KEY)
	if err != nil {
		t.Fatal(err)
	}
	t.Log("rangeCount deleted", resp.Deleted)

	times := 100
	m := make(map[string]struct{})
	stop := make(chan struct{})
	ch := make(chan string)
	go func() {
		for u := range ch {
			m[u] = struct{}{}
		}
		stop <- struct{}{}
	}()

	eg, _ := errgroup.WithContext(context.Background())
	if err != nil {
		t.Fatal(err)
	}

	for i := 0; i < times; i++ {
		eg.Go(func() error {
			url, err := tu.ShortUrl()
			t.Log(url)
			ch <- url
			return err
		})
	}
	if err := eg.Wait(); err != nil {
		t.Log("errgroup error", err)
	}
	close(ch)
	<-stop
	Convey("TestShortUrl", t, func() {
		So(len(m), ShouldEqual, times)
	})
}

func TestCreateRange(t *testing.T) {
	appName := "test3"
	tu, err := NewTinyUrl(appName, []string{"127.0.0.1:2379"})
	if err != nil {
		t.Fatal(err)
	}

	s, err := concurrency.NewSession(tu.etcdCli)
	if err != nil {
		t.Fatal(err)
	}

	prefixKey := tinyurl.ETCD_COUNTER_URL + appName
	resp, err := tu.etcdCli.Delete(tu.ctx, prefixKey, etcdClient.WithPrefix())
	if err != nil {
		t.Fatal(err)
	}
	t.Log("Deleted", resp.Deleted)

	resp, err = tu.etcdCli.Delete(tu.ctx, tinyurl.ETCD_RANGE_COUNT_KEY)
	if err != nil {
		t.Fatal(err)
	}
	t.Log("rangeCount deleted", resp.Deleted)

	_, err = tu.etcdCli.Put(tu.ctx, tinyurl.ETCD_RANGE_COUNT_KEY, "0")
	if err != nil {
		t.Fatal(err)
	}

	times := 100
	stop := make(chan struct{})
	ch := make(chan *createRangeResult)
	m := make(map[int64]*createRangeInfo)

	go func() {
		for info := range ch {
			if _, ok := m[info.rangeIndex]; !ok {
				m[info.rangeIndex] = new(createRangeInfo)
			}
			if info.err != nil {
				m[info.rangeIndex].fail++
			} else {
				m[info.rangeIndex].success++
			}
			m[info.rangeIndex].versions = append(m[info.rangeIndex].versions, info.version)
		}
		stop <- struct{}{}
	}()

	eg, _ := errgroup.WithContext(context.Background())
	if err != nil {
		t.Fatal(err)
	}

	for i := 0; i < times; i++ {
		eg.Go(func() error {
			info := tu.createRange(s.Lease())
			t.Logf("info:%+v\n", info)

			if info.err != nil {
				if info.err != tinyurl.ETCD_CAS_ERR {
					return info.err
				}
			}
			ch <- info
			return nil
		})
	}
	if err := eg.Wait(); err != nil {
		t.Log("errgroup error", err)
	}
	close(ch)
	<-stop

	Convey("TestCreateRange", t, func() {
		var sum int64
		for rangeIndex, info := range m {
			So(info.success, ShouldEqual, 1)

			sum += (info.success + info.fail)

			size := len(info.versions)
			sort.Slice(info.versions, func(i, j int) bool { return info.versions[i] < info.versions[j] })

			So(info.versions, ShouldResemble, genExpectVersions(size))

			t.Log(rangeIndex, info)
		}
		So(sum, ShouldEqual, times)
	})
}

type createRangeInfo struct {
	success, fail int64
	versions      []int64
}

func genExpectVersions(n int) []int64 {
	versions := make([]int64, n)
	var i int64
	for i = 0; i < int64(n); i++ {
		versions[i] = i + 1
	}
	return versions
}

func TestGetRangeCount(t *testing.T) {
	appName := "test4"
	tu, err := NewTinyUrl(appName, []string{"127.0.0.1:2379"})
	if err != nil {
		t.Fatal(err)
	}

	Convey("TestGetRangeCount", t, func() {
		Convey("不存在rangeCount", func() {
			resp, err := tu.etcdCli.Delete(tu.ctx, tinyurl.ETCD_RANGE_COUNT_KEY)
			if err != nil {
				t.Fatal(err)
			}
			t.Log("deleted", resp.Deleted)

			count, err := tu.getRangeCount()
			So(count, ShouldEqual, 0)
			So(err, ShouldBeNil)
		})
		Convey("存在rangeCount", func() {
			_, err := tu.etcdCli.Put(tu.ctx, tinyurl.ETCD_RANGE_COUNT_KEY, fmt.Sprintf("%d", 10))
			if err != nil {
				t.Fatal(err)
			}

			count, err := tu.getRangeCount()
			So(count, ShouldEqual, 10)
			So(err, ShouldBeNil)
		})
	})
}
