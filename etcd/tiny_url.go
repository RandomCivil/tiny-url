package etcd

import (
	"context"
	"fmt"
	"strconv"
	"strings"

	tinyurl "gitlab.com/RandomCivil/tiny-url"
	"gitlab.com/RandomCivil/tiny-url/core"
	etcdClient "go.etcd.io/etcd/client/v3"
	"go.etcd.io/etcd/client/v3/concurrency"
)

type tinyUrl struct {
	etcdCli *etcdClient.Client
	ctx     context.Context
	appName string
}

func NewTinyUrl(appName string, endpoints []string) (*tinyUrl, error) {
	cli, err := etcdClient.New(etcdClient.Config{
		Endpoints: endpoints,
	})
	if err != nil {
		return nil, err
	}
	return &tinyUrl{
		appName: appName,
		etcdCli: cli,
		ctx:     context.Background(),
	}, nil
}

func (t *tinyUrl) ShortUrl() (string, error) {
	s, err := concurrency.NewSession(t.etcdCli)
	if err != nil {
		return "", err
	}

	curRangePath, err := t.getCurRange()
	if err != nil {
		return "", err
	}

	put := etcdClient.OpPut(curRangePath, "", etcdClient.WithLease(s.Lease()))
	getOwner := etcdClient.OpGet(curRangePath, etcdClient.WithMaxModRev(0))
	resp, err := t.etcdCli.Txn(t.ctx).Then(put, getOwner).Commit()
	if err != nil {
		return "", err
	}

	kvs := resp.Responses[1].GetResponseRange().Kvs
	fmt.Println(kvs[len(kvs)-1].ModRevision, kvs[len(kvs)-1].Version)

	var curRange, version int64
	if kvs[len(kvs)-1].Version > tinyurl.LIMIT_PER_RANGE {
		info := t.createRange(s.Lease())
		curRange = info.rangeIndex
		version = info.version
	} else {
		curRange, err = t.parseCurRange(curRangePath)
		if err != nil {
			return "", err
		}
		version = kvs[len(kvs)-1].Version
	}
	count := (curRange-1)*tinyurl.LIMIT_PER_RANGE + version
	return core.Base62(count - 1), nil
}

func (t *tinyUrl) parseCurRange(curRangePath string) (rangeIndex int64, err error) {
	rangeSplit := strings.Split(curRangePath, "_")
	rangeIndex, err = strconv.ParseInt(rangeSplit[1], 10, 0)
	if err != nil {
		return
	}
	return
}

type createRangeResult struct {
	version    int64
	rangeIndex int64
	err        error
}

func (t *tinyUrl) createRange(lease etcdClient.LeaseID) (info *createRangeResult) {
	info = new(createRangeResult)
	// rangeCount可能是已经increase的,导致多创建range.不过后面只会在最新的range上操作，所以最多损失部分range
	rangeCount, err := t.getRangeCount()
	if err != nil {
		info.err = err
		return
	}

	newRangeCountKey := fmt.Sprintf(tinyurl.ETCD_COUNTER_URL+t.appName+"/range_%d", rangeCount+1)
	cmp := etcdClient.Compare(etcdClient.Value(tinyurl.ETCD_RANGE_COUNT_KEY), "=", fmt.Sprintf("%d", rangeCount))
	putRangeCount := etcdClient.OpPut(tinyurl.ETCD_RANGE_COUNT_KEY, fmt.Sprintf("%d", rangeCount+1))
	putRange := etcdClient.OpPut(newRangeCountKey, "", etcdClient.WithLease(lease))
	getOwner := etcdClient.OpGet(newRangeCountKey, etcdClient.WithMaxModRev(0))

	resp, err := t.etcdCli.Txn(t.ctx).If(cmp).Then(putRange, getOwner, putRangeCount).Else(putRange, getOwner).Commit()
	if err != nil {
		info.err = err
		return
	}

	info.rangeIndex = rangeCount + 1
	kvs := resp.Responses[1].GetResponseRange().Kvs
	if !resp.Succeeded {
		info.version = kvs[len(kvs)-1].Version
		info.err = tinyurl.ETCD_CAS_ERR
		return
	}

	info.version = kvs[len(kvs)-1].Version
	return
}

func (t *tinyUrl) getRangeCount() (int64, error) {
	resp, err := t.etcdCli.Get(t.ctx, tinyurl.ETCD_RANGE_COUNT_KEY, etcdClient.WithMaxModRev(0))
	if err != nil {
		return 0, err
	}

	if len(resp.Kvs) == 0 {
		return 0, nil
	}

	rangeCount, err := strconv.ParseInt(string(resp.Kvs[0].Value), 10, 64)
	if err != nil {
		return 0, err
	}
	return rangeCount, nil
}

func (t *tinyUrl) getCurRange() (string, error) {
	resp, err := t.etcdCli.Get(t.ctx, tinyurl.ETCD_COUNTER_URL+t.appName, etcdClient.WithPrefix(), etcdClient.WithSort(etcdClient.SortByKey, etcdClient.SortDescend))
	if err != nil {
		return "", err
	}

	if len(resp.Kvs) == 0 {
		_, err := t.etcdCli.Put(t.ctx, tinyurl.ETCD_RANGE_COUNT_KEY, "1")
		if err != nil {
			return "", err
		}
		return tinyurl.ETCD_COUNTER_URL + t.appName + "/range_1", nil
	}
	return string(resp.Kvs[0].Key), nil
}
