package zk

import (
	"context"
	"fmt"
	"testing"
	"time"

	"github.com/go-zookeeper/zk"
	. "github.com/smartystreets/goconvey/convey"
	tinyurl "gitlab.com/RandomCivil/tiny-url"
	"golang.org/x/sync/errgroup"
)

func TestNew(t *testing.T) {
	// app range map存在（一个app并发创建）
	// app range map不存在
	//     range存在（两个不同app并发）
	//     range不存在
	//         seq并发创建

	appName := "test-app"
	s := []string{"127.0.0.1"}
	Convey("TestNew", t, func() {
		Convey("一个app并发创建app range map", func() {
			conn, _, err := zk.Connect([]string{"127.0.0.1"}, time.Second*1000)
			if err != nil {
				panic(err)
			}
			defer conn.Close()

			deleteAll(conn, tinyurl.ZK_APPNAME_RANGE_MAP_URL+"/"+appName)
			deleteAll(conn, tinyurl.ZK_COUNTER_URL)
			_, err = conn.Create(tinyurl.ZK_COUNTER_URL, make([]byte, 0), 0, zk.WorldACL(zk.PermAll))
			if err != nil {
				panic(err)
			}

			ch := make(chan newInfo, 3)
			for i := 0; i < 3; i++ {
				go func(i int) {
					tu, err := newTinyUrl(s, appName)
					t.Log("hehe", err)
					ch <- newInfo{
						i:   i,
						err: err,
						t:   tu,
					}
				}(i)
			}

			var successNum, errNum int
			var successTu *tinyUrl
			for i := 0; i < 3; i++ {
				if info := <-ch; info.err != nil {
					errNum++
				} else {
					successTu = info.t
					successNum++
				}
			}
			So(successNum, ShouldEqual, 1)
			So(errNum, ShouldEqual, 2)

			_, stat, err := successTu.conn.Get(tinyurl.ZK_COUNTER_URL)
			if err != nil {
				panic(err)
			}
			So(stat.NumChildren, ShouldEqual, 1)

			d, _, err := successTu.conn.Get(tinyurl.ZK_APPNAME_RANGE_MAP_URL + "/" + appName)
			if err != nil {
				panic(err)
			}
			So(string(d), ShouldEqual, "/counter/range_1")
		})
		Convey("并发创建range", func() {
			conn, _, err := zk.Connect([]string{"127.0.0.1"}, time.Second*1000)
			if err != nil {
				panic(err)
			}
			defer conn.Close()

			deleteAll(conn, tinyurl.ZK_APPNAME_RANGE_MAP_URL+"/"+appName)
			deleteAll(conn, tinyurl.ZK_COUNTER_URL)
			_, err = conn.Create(tinyurl.ZK_COUNTER_URL, make([]byte, 0), 0, zk.WorldACL(zk.PermAll))
			if err != nil {
				panic(err)
			}

			tu, err := newTinyUrl(s, appName)
			if err != nil {
				panic(err)
			}

			batchShortUrl(tu, tinyurl.LIMIT_PER_RANGE, t)
			batchShortUrl(tu, 10, t)
		})
	})
}

func batchShortUrl(tu *tinyUrl, n int, t *testing.T) {
	eg, _ := errgroup.WithContext(context.Background())
	ch := make(chan string)
	stop := make(chan struct{})
	m := make(map[string]struct{})
	go func(tt *testing.T) {
		for {
			select {
			case s := <-ch:
				m[s] = struct{}{}
			case <-stop:
				Convey("检查创建的seq数", tt, func() {
					So(len(m), ShouldEqual, n)
				})
			}
		}
	}(t)
	for i := 0; i < n; i++ {
		eg.Go(func() error {
			result, err := tu.ShortUrl()
			t.Log("hehe", result, err)
			if len(result) > 0 {
				ch <- result
			}
			return err
		})
	}
	if err := eg.Wait(); err == nil {
		t.Log("finished")
	} else {
		t.Fatal(err)
	}
	stop <- struct{}{}
}

type newInfo struct {
	i   int
	err error
	t   *tinyUrl
}

func deleteAll(conn *zk.Conn, path string) {
	paths, _, err := conn.Children(path)
	if err != nil {
		if err == zk.ErrNoNode {
			return
		}
		panic(err)
	}
	fmt.Println("paths", paths, path)
	for _, p := range paths {
		deleteAll(conn, path+"/"+p)
	}
	if err := conn.Delete(path, -1); err != nil {
		panic(err)
	}
}

func TestParseUrl(t *testing.T) {
	parseUrl("/counter/range_3", "/counter/range_3/n_0000000004")
}
