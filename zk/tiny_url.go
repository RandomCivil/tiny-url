package zk

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/go-zookeeper/zk"
	tinyurl "gitlab.com/RandomCivil/tiny-url"
	"gitlab.com/RandomCivil/tiny-url/core"
)

func New(servers []string, appName string) (*tinyUrl, error) {
	tu, err := newTinyUrl(servers, appName)
	if err != nil {
		if err == zk.ErrNodeExists {
			return tu, nil
		}
		return nil, err
	}
	return tu, nil
}

type tinyUrl struct {
	conn    *zk.Conn
	appName string
}

func newTinyUrl(servers []string, appName string) (*tinyUrl, error) {
	conn, _, err := zk.Connect([]string{"127.0.0.1"}, time.Second*1000)
	if err != nil {
		return nil, err
	}
	tu := &tinyUrl{
		conn:    conn,
		appName: appName,
	}
	err = tu.register()
	if err != nil {
		return nil, err
	}
	return tu, nil
}

func (t *tinyUrl) ShortUrl() (string, error) {
	rangeUrl, _, err := t.conn.Get(tinyurl.ZK_APPNAME_RANGE_MAP_URL + "/" + t.appName)
	if err != nil {
		return "", err
	}

	seqUrl, err := t.conn.Create(string(rangeUrl)+"/n_", make([]byte, 0), zk.FlagSequence, zk.WorldACL(zk.PermAll))
	if err != nil {
		return "", err
	}
	rangeIndex, counter, err := parseUrl(string(rangeUrl), seqUrl)
	if err != nil {
		return "", err
	}
	if counter > tinyurl.LIMIT_PER_RANGE {
	stop:
		for {
			select {
			case <-time.After(3 * time.Second):
				return "", errors.New("retry create range timeout")
			default:
				if err := t.createRange(counter); err != nil {
					// 已创建，重新尝试创建新的range
					if err == zk.ErrNodeExists {
						fmt.Printf("retry short url:%d err:%+v\n", counter, err)
						continue
					}
					return "", err
				}
				break stop
			}
		}
		return t.ShortUrl()
	}

	n := (int64(rangeIndex)-1)*tinyurl.LIMIT_PER_RANGE + int64(counter)
	fmt.Printf("n:%d\n", n)
	return core.Base62(n), nil
}

func parseUrl(rangeUrl, seqUrl string) (rangeIndex, counter int, err error) {
	rangeSplit := strings.Split(rangeUrl, "_")
	rangeIndex, err = strconv.Atoi(rangeSplit[1])
	if err != nil {
		return
	}

	seqSplit := strings.Split(seqUrl, "_")
	counter, err = strconv.Atoi(seqSplit[2])
	counter++
	fmt.Printf("rangeIndex:%d counter:%d err:%+v\n", rangeIndex, counter, err)
	return
}

func (t *tinyUrl) createRange(from int) error {
	_, stat, err := t.conn.Get(tinyurl.ZK_COUNTER_URL)
	if err != nil {
		return err
	}

	fmt.Printf("counter children num:%d,from:%d\n", stat.NumChildren, from)
	newRangeUrl, err := t.conn.Create(tinyurl.ZK_COUNTER_RANGE_URL+fmt.Sprintf("%d", stat.NumChildren+1), make([]byte, 0), 0, zk.WorldACL(zk.PermAll))
	if err != nil {
		return err
	}

	_, err = t.conn.Set(tinyurl.ZK_APPNAME_RANGE_MAP_URL+"/"+t.appName, []byte(newRangeUrl), -1)
	if err != nil {
		return err
	}
	return nil
}

func (t *tinyUrl) register() (err error) {
	defer func() {
		if err != nil {
			t.conn.Close()
			fmt.Println("conn close")
		}
	}()
	_, err = t.conn.Create(tinyurl.ZK_APPNAME_RANGE_MAP_URL+"/"+t.appName, make([]byte, 0), 0, zk.WorldACL(zk.PermAll))
	if err != nil {
		return err
	}

	for {
		select {
		case <-time.After(3 * time.Second):
			return errors.New("retry create range timeout")
		default:
			if err := t.createRange(0); err != nil {
				// 已创建，重新尝试创建新的range
				if err == zk.ErrNodeExists {
					fmt.Printf("111 retry short url:%d err:%+v\n", 0, err)
					continue
				}
				return err
			}
			return nil
		}
	}
}
